# dynamicBMS

dynamicBMS is a flexible, reprogrammable, Battery Management System (BMS) based on the Renesas ISL94202 Battery Front End (BFE) IC. This chip supports battery packs from 3s to 8s with almost any chemistry type as well as I2C to communicate information about the pack to a microcontroller. The microcontroller can then be used to make decisions about cell balancing, charging, discharging, and logging. 

This project will focus on using the ISL94202 to interface between the battery pack and a microcontroller, in this case the ATtiny3216, but can theoretically use any chip assuming the pin mapping is changed correctly. The ATtiny3216 will display information about the pack on a 128x64 I2C OLED screen as well be an interface to turn on and off outputs and change settings in the EEPROM (i.e. overcharge threshold, overtemperature threshold, etc.). 

## Features

* We'll add some features to this list later. 


## Hardware

[ISL94202 Datasheet](https://www.renesas.com/www/doc/datasheet/isl94202.pdf "Link to ISL94202 Datasheet")

[Attiny3216 Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1616-3216-DataSheet-DS40001997C.pdf "Link to Attiny3216 Datasheet")

To find out more about the hardware of the BMS click [here.](hardware/hardware.md)

## Software

[megaTinyCore GitHub](https://github.com/SpenceKonde/megaTinyCore "Link to megaTinyCore GitHub")

[u8g2 GitHub](https://github.com/olikraus/u8g2 "Link to u8g2 Library GitHub")


To find out more about the software of the BMS click [here.](hardware/software.md)

/*
  created 20 Feb 2020
  by Nathaniel Belles

  This sketch fades the DAC output from low to high and back to low.
  This sketch was written for the ATtiny 212/412 (where DAC ouptput is on pin 0).
*/

// These constants won't change. They're used to give names to the pins used:
const int pin = 0;  //Change this to the DAC output pin on whatever microcontroller being used. 

void setup() {
  // set output pin as output.
    pinMode(pin, OUTPUT);
}

void loop() {
  // iterate over the pin:

    for (int brightness = 0; brightness < 255; brightness++) {
      analogWrite(pin, brightness);
      delay(10);
    }
    // fade the LED on thisPin from brightest to off:
    for (int brightness = 255; brightness >= 0; brightness--) {
      analogWrite(pin, brightness);
      delay(10);
    }
    
}

/*
  created 20 Feb 2020
  by Nathaniel Belles
  
  This sketch shows how an input can be used both for logic blocks and digital reads simultaneously
  This sketch was written for the ATtiny 212/412 (where DAC ouptput is on pin 0).
*/

#include <Logic.h>

void setup()
{
  // Setup logic block 0
  // Logic block 0 has three inputs, PA0, PA1 and PA2.
  Logic0.enable = true;               // Enable logic block 0
  Logic0.input0 = in::input;          // Set PA0 as input
  Logic0.input1 = in::input;          // Set PA1 as input
  Logic0.input2 = in::input;          // Set PA2 as input
  Logic0.output = out::enable;        // Enable logic block 0 output pin (PA3)
  Logic0.filter = filter::disable;    // No output filter enabled
  Logic0.truth = B00110011;           // Set truth table (equivalent to !PA1)

  // Initialize logic block 0
  Logic0.init();

  // Start the AVR logic hardware
  Logic::start();

  // Initialize normal sketch pins 
  pinMode(2, INPUT);
  pinMode(1, OUTPUT);
}

void loop()
{
  // Set output pin equal to the value of an input pin that is also being used for logic block
  digitalWrite(1, digitalRead(2));
  delay(1);

}
